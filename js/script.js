$('#filter-price').change(() => {
    showData(data)
})

$('#filter-cap').change(() => {
    showData(data)
})

function formatNumber(num, isCap = false) {
    if (isCap) {
        if (num > 1000000000) {
            return `$${Math.round(num / 1000000000).toFixed(2)}B`
        } else if (num > 1000000) {
            return `$${Math.round(num / 1000000).toFixed(2)}M`
        } else {
            return `$${num.toFixed(2)}`
        }
    } else {
        if (num > 0.1) {
            return `$${num.toFixed(2)}`
        } else {
            return `$${num.toFixed(8)}`
        }
    }

}

function showWatchTop() {
    const watchTopDatas = []
    for (let i = 0; i < 4; i++) {
        watchTopDatas.push(data[i])
    }

    const newHtml = watchTopDatas.map((item) => (
        `<div class="col-md-3 col-sm-6 p-2">
            <div class="top-chart border">
                <div class="row">
                    <div class="col-6">
                        <div class="b text-black-50 ml-2 mt-2">
                            <img class="icon mr-2" src="https://files.bitscreener.com/static/img/coins/32x32/${item.slug}.png">${item.name}
                        </div>
                    </div>
                    <div class="col-6">
                        <p>${formatNumber(item.price)}</p>
                        <span class="${item.p_24h >= 0 ? "text-danger" : "text-success"}">${formatNumber(Math.abs(item.p_24h), true).replace("$", '')}%</span>
                    </div>
                </div>

                <div class="text-center my-2">
                    <img src="./images/b-chart.png" alt="chart ${item.name} 7 days"><br />
                </div>

                <div class="text-right">
                    <time>24h</time>
                </div>
            </div>
        </div>`
    ))

    $('#watchtop-coin').empty().append(newHtml)
}

$(document).ready(function () {
    showData(data, 'cap', true)

    showWatchTop()

    $('.navbar input[type="search"]').keyup(function (e) {
        const searchTxt = this.value.toLocaleLowerCase();
        const resultBox = $('.search-result')
        if (!searchTxt) {
            resultBox.addClass('d-none')
            return;
        }

        resultBox.empty()
        const result = []

        for (let i = 0; i < data.length; i++) {
            const item = data[i];
            const name = item.name.toLocaleLowerCase()
            if (name.indexOf(searchTxt) >= 0) {
                result.push(item)
            }
        }

        for (let i = 0; i < result.length; i++) {
            const item = result[i]
            resultBox.append('<li class="list-group-item">' + item.name + '</li>');
        }

        resultBox.removeClass('d-none')
    })
})

function sort(field, el) {
    $(el).toggleClass('desc')

    const isDesc = $(el).hasClass('desc');
    showData(data, field, !isDesc)
}

function showData(inputData, sortType, desc) {

    if (sortType && desc !== undefined) {
        inputData.sort((o1, o2) => (desc ? -1 : 1) * (o1[sortType] - o2[sortType]))
    }

    let items = filterByPrice(inputData)

    items = filterByCap(items);

    const len = items.length
    $('table.stats tbody').empty()
    $('#title-screener').empty().append(`<h3 class="text-center my-4">Screening ${len} Coins</h3>`);
    for (let i = 0; i < len; i++) {
        const item = items[i]

        const p = formatNumber(item.price)
        const vlm = formatNumber(item.vlm, true)
        const p_24h = item.p_24h
        const p24h = formatNumber(Math.abs(item.p_24h), true)

        const cl = p_24h < 0 ? 'text-danger' : 'text-success'

        const cap = formatNumber(item.cap, true)

        const html = `<tr>
                <th><img class="star" src="./images/star-white.png" onclick="star(this)"></th>
                <td class="stt">${i + 1}</td>
                <td>
                    <div>
                        <img class="icon" src="https://files.bitscreener.com/static/img/coins/32x32/${item.slug}.png">
                        <a href="https://red-snow-5247.animaapp.io/artboard1?fbclid=IwAR1B3WC8SB8C-TDybpTdhi2Bt9Y9uKlWaqPPTm2eUG_avsDLnSz1z1UgzrA" class="coin-name">${item.name}</a>
                    </div>
                </td>
                <td class="t-17 pl-5p">${p}</td>
                <td class="t-17 pl-5p">${vlm}</td>
                <td class="t-17 pl-3p ${cl}">${p24h.replace('$', '')}%</td>
                <td class="t-17 pl-5p">${cap}</td>
                <td class="text-center">
                    <img alt="chart ${item.name} 7 days" class="chart" src="./images/d-chart.png">
                </td>
            </tr>`

        $('table.stats tbody').append(html)
    }
}

function filterQuery(item, field, min, max) {
    if (min && max) {
        return item[field] >= min && item[field] <= max
    } else if (min) {
        return item[field] >= min
    } else if (max) {
        return item[field] <= max
    } else {
        return true
    }
}

function filterByPrice(inputData) {

    const { min, max } = getPriceCondition()

    return inputData.filter((item) => {
        return filterQuery(item, 'price', min, max)
    })
}

function getPriceCondition() {
    const val = parseInt($('#filter-price').val())
    let max = null
    let min = null
    switch (val) {
        case 1:
            min = 100
            break
        case 2:
            min = 50
            max = 100
            break
        case 3:
            min = 10
            max = 50
            break
        case 4:
            min = 1
            max = 10
            break
        case 5:
            max = 1
            break
        default:
            break
    }

    return { min, max }
}

function filterByCap(inputData) {
    const { min, max } = getCapCondition()

    return inputData.filter((item) => {
        return filterQuery(item, 'cap', min, max)
    })
}

function getCapCondition() {
    const val = parseInt($('#filter-cap').val())
    let max = null
    let min = null
    switch (val) {
        case 1:
            min = 1000000000
            break
        case 2:
            min = 100000000
            max = 1000000000
            break
        case 3:
            max = 100000000
            break
        default:
            break
    }

    return { min, max }
}

function star(el) {
    const $this = $(el);

    if (!$this.hasClass('active')) {
        el.src = './images/star.png'
    } else {
        el.src = './images/star-white.png'
    }

    $this.toggleClass('active');
}